# coding:utf-8
import base64
from Crypto.Util.Padding import pad
from Crypto.Cipher import AES
import math
import random

def _gas(aes_str, key, iv):
    aes = AES.new(key.encode('utf-8'), AES.MODE_CBC, iv.encode('utf-8'))
    pad_pkcs7 = pad(aes_str.encode('utf-8'), AES.block_size,
                      style='pkcs7')  # 选择pkcs7补全
    encrypt_aes = aes.encrypt(pad_pkcs7)
    # 加密结果
    encrypted_text = str(base64.encodebytes(encrypt_aes),
                         encoding='utf-8')  # 解码

    encrypted_text_str = encrypted_text.replace("\n", "")
    return encrypted_text_str


def _rds(_len):
    _chars = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678"
    _chars_len = len(_chars)
    retStr = ""
    for i in range(_len):
        retStr += _chars[math.floor(random.random() * _chars_len)]
    return retStr

def encryptAES(data, _p1):
  return _gas(_rds(64) + data, _p1, _rds(16))


if __name__ == '__main__':
    key = "LfF3gCwSIhWU2WTA"
    kk = _gas(_rds(64) + "123456", key, _rds(16))

    print(kk, len(kk))
    # key的长度需要补长(16倍数),补全方式根据情况而定,此处我就手动以‘0’的方式补全的32位key
    # key字符长度决定加密结果,长度16：加密结果AES(128),长度32：结果就是AES(256)
    # key = "ABCDEFGHIJKLMN000000000000000000"
    # # 加密字符串长同样需要16倍数：需注意,不过代码中pad()方法里，帮助实现了补全（补全方式就是pkcs7）
    # aes_str = "abc"
    # encryption_result = aes_cipher(key, aes_str)
    # print(encryption_result)
