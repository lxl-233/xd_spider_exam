#!/bin/bash
#xd_exam.sh

PID=$(docker ps -a -q -f name=xd_exam)
if [ -z "$PID" ]
then
	echo xd_exam container is non-exist
else
	docker stop $PID
	docker rm $PID
fi

IMAGE_ID=$(docker images -q xd_exam:v1.0)

if [ -z "$IMAGE_ID" ]
then
	echo xd_exam:v1.0 images is non-exist
else
	docker rmi -f $IMAGE_ID
fi

docker build -t xd_exam:v1.0 .
docker run -d  -p 8283:5000 --name="xd_exam" xd_exam:v1.0 
