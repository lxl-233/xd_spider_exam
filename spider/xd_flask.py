from flask import Flask
from flask import request
import xd_exam
import json
app = Flask(__name__)


@app.route("/api/login")
def login():
    username = request.args.get("username")
    password = request.args.get("password")
    ss = xd_exam.login(username, password)
    return json.dumps(ss)



if __name__ == "__main__":
    app.run(host="0.0.0.0")
    #app.run()
