'use strict'
const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },

  devServer: {
    proxy: {
      '/api': {
        // target: 'http://47.93.9.196:5002/',
        target: 'http://121.37.142.54:8283/',
        ws: true,
        changeOrigin: true
      },
      '/ehall_api': {
        target: 'http://ehall.xidian.edu.cn/',
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          // 路径重写
          '/ehall_api': ''
        },
        headers: {
          referer:
            'http://ehall.xidian.edu.cn/gsapp/sys/wdcjapp/*default/index.do',
          origin: 'http://ehall.xidian.edu.cn'
        }
      }
    }
  }
}
