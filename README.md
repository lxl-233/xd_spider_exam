# 西电考试成绩爬虫

> 主要通过用户输入成绩来检索课程，如果课程成绩相等，就会出现对应的课程。

- 项目在线地址 [xd_spider_exam](http://47.93.9.196:8211/#/)
- 前端 `Vue` 
- 后端 `flask`
- 爬虫 `python requests`库

界面如下：

![image-20210117171548026](http://qiniu.xiaolongli.top//img/image-20210117171548026.png)

## 项目关键点

> 对西电一站式登录的模拟，需要绕过登录的 `CAS` 验证, 这个一开始想着用前端模拟，结果不行，需要保持session会话，只能python来尝试。

python爬虫的相关代码在 [spider](https://gitee.com/lxl-233/xd_spider_exam/tree/master/spider)

##  项目下载

> 推荐使用`git`克隆

```shell
git clone https://gitee.com/lxl-233/xd_spider_exam
```

###  项目启动

安装用到的依赖

```
npm install
```

启动服务

```
npm run serve
```

打包项目

```
npm run build
```

