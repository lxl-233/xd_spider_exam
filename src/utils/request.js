import axios from 'axios'
import { Message } from 'element-ui'
// import router from '@/router'
/**
 * 模拟登录
 * 如果请求失败， 需要把 csrftoken 和 sessionid 两个值换掉
 */

// create an axios instance
const service = axios.create({
  // baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  withCredentials: true // send cookies when cross-domain requests
})

// 请求拦截器
service.interceptors.request.use(
  config => {
    return config
  },
  error => {
    // do something with request error
    // console.log(error) // for debug
    return Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
  res => {
    // if the custom code is not 200, it is judged as an error.
    if (res.status >= 300) {
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    return Promise.reject(error)
  }
)

export default service
