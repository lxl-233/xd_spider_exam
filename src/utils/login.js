import request from './request'
import Cookie from 'js-cookie'

export function login(data1) {
  return request({
    url: '/api/login',
    method: 'GET',
    params: data1
  })
}

export function check_login() {
  return request({
    url: '/ehall_api/gsapp/sys/wdcjapp/*default/index.do#/wdcj',
    method: 'GET'
  })
}

export function api_serach(value) {
  // const data = {
  //   'querySetting': JSON.stringify([
  //     {
  //       'name': 'CJXSZ',
  //       'caption': '成绩显示值',
  //       'linkOpt': 'AND',
  //       'builderList': 'cbl_String',
  //       'builder': 'equal',
  //       'value': value.toString()
  //     }
  //   ]),
  //   '_gotoFirstPage': 'true'
  // }
  const params = new URLSearchParams()
  params.append(
    'querySetting',
    JSON.stringify([{
      'name': 'CJXSZ',
      'caption': '成绩',
      'linkOpt': 'AND',
      'builderList': 'cbl_String',
      'builder': 'equal',
      'value': value.toString()
    }])
  )
  params.append('pageSize', '20')
  params.append('pageNumber', '1')
  Cookie.set('_WEU', Cookie.get('copy_WEU'))
  return request({
    url: '/ehall_api/gsapp/sys/wdcjapp/modules/wdcj/xscjcx.do',
    method: 'POST',
    data: params,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    }
  })
}

